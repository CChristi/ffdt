import {combineEpics} from 'redux-observable';
import userManagementEpics from 'root/features/user-management/epics';

const rootEpic = combineEpics(userManagementEpics);

export default rootEpic;
