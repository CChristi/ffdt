import {createAction} from '@reduxjs/toolkit';

const appLaunched = createAction('APP_LAUNCHED');

export {appLaunched};
