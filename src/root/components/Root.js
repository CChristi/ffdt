import {BrowserRouter} from 'react-router-dom';
import Footer from 'root/features/layout/components/Footer';
import Header from 'root/features/layout/components/Header';
import AccessControl from 'root/features/user-management/containers/AccessControl';
import config from 'config';
import Routes from 'root/routes';

function Root() {
  return (
    <div className="App">
      <Header />
      <BrowserRouter>
        <AccessControl {...config.accessControl}>
          <Routes />
        </AccessControl>
      </BrowserRouter>
      <Footer />
    </div>
  );
}

export default Root;
