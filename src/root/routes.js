import {Switch} from 'react-router-dom';
import UserManagementRoutes from 'root/features/user-management/routes';
import LayoutRoutes from 'root/features/layout/routes';

const Routes = () => (
  <Switch>{[...UserManagementRoutes, ...LayoutRoutes]}</Switch>
);

export default Routes;
