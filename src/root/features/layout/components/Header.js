import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import LogoutButtonContainer from 'root/features/user-management/containers/LogoutButtonContainer';

const Header = () => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" component="div" sx={{flexGrow: 1}}>
        Frontend Developer Trial Task
      </Typography>
      <LogoutButtonContainer />
    </Toolbar>
  </AppBar>
);

export default Header;
