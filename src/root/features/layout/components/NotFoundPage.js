import {Typography} from '@mui/material';
import Container from '@mui/material/Container';

const NotFoundPage = () => (
  <Container
    component="main"
    maxWidth="xs"
    sx={{my: {xs: 3, md: 6}, p: {xs: 2, md: 3}}}
  >
    <Typography component="h1" variant="h4" align="center">
      Not Found
    </Typography>
  </Container>
);

export default NotFoundPage;
