import {Route} from 'react-router-dom';
import NotFoundPage from 'root/features/layout/components/NotFoundPage';

const LayoutRoutes = [
  <Route key="not-found-page" path="/" component={NotFoundPage} />,
];

export default LayoutRoutes;
