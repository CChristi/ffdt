import {prop} from 'packages/fp';
import {compose} from 'redux';

const userManagement = prop('userManagement');
const isUserLoading = compose(prop('isUserLoading'), userManagement);
const isJwtLoading = compose(prop('isJwtLoading'), userManagement);
const user = compose(prop('user'), userManagement);
const userId = compose(prop('id'), user);
const userEmail = compose(prop('email'), user);
const userFirstName = compose(prop('firstName'), user);
const userLastName = compose(prop('lastName'), user);

export {
  user,
  userId,
  userEmail,
  userFirstName,
  userLastName,
  isUserLoading,
  isJwtLoading,
};
