import {createAction} from '@reduxjs/toolkit';

const loginFormSubmitted = createAction('LOGIN_FORM_SUBMITTED');
const loginCreated = createAction('LOGIN_CREATED');
const loginFailed = createAction('LOGIN_FAILED');

const authLinkInitialized = createAction('AUTH_LINK_INITIALED');

const userFetchSuccess = createAction('USER_FETCH_SUCCESS');
const userFetchError = createAction('USER_FETCH_ERROR');

const logoutButtonClicked = createAction('LOGOUT_BUTTON_CLICKED');

export {
  loginFormSubmitted,
  loginCreated,
  loginFailed,
  authLinkInitialized,
  userFetchSuccess,
  userFetchError,
  logoutButtonClicked,
};
