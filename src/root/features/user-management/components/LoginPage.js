import Container from '@mui/material/Container';
import LoginFormContainer from 'root/features/user-management/containers/LoginFormContainer';

const LoginPage = () => (
  <Container component="main" maxWidth="xs">
    <LoginFormContainer />
  </Container>
);

export default LoginPage;
