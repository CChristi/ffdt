import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const UserDetails = ({id, email, firstName, lastName}) => (
  <Paper variant="outlined" sx={{my: {xs: 3, md: 6}, p: {xs: 2, md: 3}}}>
    <Typography component="h1" variant="h4" align="center">
      Profile
    </Typography>
    <List disablePadding>
      <ListItem sx={{py: 1, px: 0}}>
        <ListItemText primary="Identifier" />
        <Typography variant="body2">{id}</Typography>
      </ListItem>
      <ListItem sx={{py: 1, px: 0}}>
        <ListItemText primary="Email" />
        <Typography variant="body2">{email}</Typography>
      </ListItem>
      <ListItem sx={{py: 1, px: 0}}>
        <ListItemText primary="First name" />
        <Typography variant="body2">{firstName}</Typography>
      </ListItem>
      <ListItem sx={{py: 1, px: 0}}>
        <ListItemText primary="Last name" />
        <Typography variant="body2">{lastName}</Typography>
      </ListItem>
    </List>
  </Paper>
);

UserDetails.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  email: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
};

export default UserDetails;
