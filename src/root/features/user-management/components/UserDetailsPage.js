import Container from '@mui/material/Container';
import UserDetailsContainer from 'root/features/user-management/containers/UserDetailsContainer';

const UserDetailsPage = () => (
  <Container component="main" maxWidth="xs">
    <UserDetailsContainer />
  </Container>
);

export default UserDetailsPage;
