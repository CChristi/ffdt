import {combineReducers, createReducer} from '@reduxjs/toolkit';
import {
  authLinkInitialized,
  loginCreated,
  loginFailed,
  loginFormSubmitted,
  logoutButtonClicked,
  userFetchError,
  userFetchSuccess,
} from 'root/features/user-management/actions';

const user = createReducer(null, {
  [userFetchSuccess]: (state, {payload: {user}}) => user,
  [logoutButtonClicked]: () => null,
});

const isJwtLoading = createReducer(false, {
  [loginFormSubmitted]: () => true,
  [loginCreated]: () => false,
  [loginFailed]: () => false,
});

const isUserLoading = createReducer(false, {
  [authLinkInitialized]: () => true,
  [userFetchSuccess]: () => false,
  [userFetchError]: () => false,
});

const userManagementReducer = combineReducers({
  user,
  isUserLoading,
  isJwtLoading,
});

export default userManagementReducer;
