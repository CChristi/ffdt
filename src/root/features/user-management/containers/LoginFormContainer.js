import PropTypes from 'prop-types';
import LoginForm from 'root/features/user-management/components/LoginForm';
import {useCallback, useState} from 'react';
import {connect, useSelector} from 'react-redux';
import {loginFormSubmitted} from 'root/features/user-management/actions';
import {isJwtLoading} from 'root/features/user-management/selectors';

const LoginFormContainer = ({loginFormSubmitted}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const isLoading = useSelector(isJwtLoading);

  const onEmailChange = useCallback(event => {
    setEmail(event.target.value);
  }, []);

  const onPasswordChange = useCallback(event => {
    setPassword(event.target.value);
  }, []);

  const handleSubmit = useCallback(
    event => {
      event.preventDefault();
      loginFormSubmitted({email, password});
    },
    [loginFormSubmitted, email, password]
  );

  return (
    <LoginForm
      email={email}
      password={password}
      isLoading={isLoading}
      onEmailChange={onEmailChange}
      onPasswordChange={onPasswordChange}
      onSubmit={handleSubmit}
    />
  );
};

LoginFormContainer.propTypes = {
  loginFormSubmitted: PropTypes.func.isRequired,
};

export default connect(null, {loginFormSubmitted})(LoginFormContainer);
