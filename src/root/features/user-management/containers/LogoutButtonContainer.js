import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import {useSelector, connect} from 'react-redux';
import {user as userSelector} from 'root/features/user-management/selectors';
import {logoutButtonClicked} from 'root/features/user-management/actions';

const LogoutButtonContainer = ({logoutButtonClicked}) => {
  const user = useSelector(userSelector);

  if (!user) {
    return null;
  }

  return (
    <Button color="inherit" onClick={logoutButtonClicked}>
      Logout
    </Button>
  );
};

LogoutButtonContainer.propTypes = {
  logoutButtonClicked: PropTypes.func.isRequired,
};

export default connect(null, {logoutButtonClicked})(LogoutButtonContainer);
