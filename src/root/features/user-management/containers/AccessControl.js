import PropTypes from 'prop-types';
import {useLocation, Redirect} from 'react-router-dom';
import {
  isUserLoading as isUserLoadingSelector,
  user as userSelector,
} from 'root/features/user-management/selectors';
import {useSelector} from 'react-redux';

const AccessControl = ({
  loginRoute,
  loginRedirect,
  securedRoutes,
  children,
}) => {
  const location = useLocation();
  const {pathname} = location;
  const user = useSelector(userSelector);
  const isUserLoading = useSelector(isUserLoadingSelector);

  if (isUserLoading) {
    return null;
  }

  if (user && pathname === loginRoute) {
    return <Redirect to={loginRedirect} />;
  }

  const isSecured = securedRoutes.some(routeMatch => routeMatch.test(pathname));

  if (isSecured && !user) {
    return <Redirect to={loginRoute} />;
  }

  return children;
};

AccessControl.propTypes = {
  loginRoute: PropTypes.string.isRequired,
  loginRedirect: PropTypes.string.isRequired,
  securedRoutes: PropTypes.arrayOf(PropTypes.object).isRequired,
  children: PropTypes.node,
};

export default AccessControl;
