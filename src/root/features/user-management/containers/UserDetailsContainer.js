import UserDetails from 'root/features/user-management/components/UserDetails';
import {useSelector} from 'react-redux';
import {
  userEmail,
  userFirstName,
  userId,
  userLastName,
} from 'root/features/user-management/selectors';

const UserDetailsContainer = () => {
  const id = useSelector(userId);
  const email = useSelector(userEmail);
  const firstName = useSelector(userFirstName);
  const lastName = useSelector(userLastName);

  return (
    <UserDetails
      id={id}
      email={email}
      firstName={firstName}
      lastName={lastName}
    />
  );
};

export default UserDetailsContainer;
