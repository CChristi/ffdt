import {Route} from 'react-router-dom';
import LoginPage from 'root/features/user-management/components/LoginPage';
import UserDetailsPage from 'root/features/user-management/components/UserDetailsPage';

const UserManagementRoutes = [
  <Route
    key="profile-page"
    path="/profile"
    exact
    component={UserDetailsPage}
  />,
  <Route key="login-page" path="/" exact component={LoginPage} />,
];

export default UserManagementRoutes;
