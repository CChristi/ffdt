import {ofType} from 'redux-observable';
import {
  authLinkInitialized,
  userFetchError,
  userFetchSuccess,
} from 'root/features/user-management/actions';
import {map, mergeMap} from 'rxjs/operators';
import {GET_USER} from 'root/features/user-management/graphql/queries';

const fetchUser = (action$, _, {apolloClient, authLink}) =>
  action$.pipe(
    ofType(authLinkInitialized),
    map(() => authLink.getDecodedPayload()?.id),
    mergeMap(async id => {
      if (!id) {
        return userFetchError(new Error('BadToken'));
      }

      try {
        const {data} = await apolloClient.query({query: GET_USER, variables: {id}});

        return userFetchSuccess(data);
      } catch (e) {
        return userFetchError(e);
      }
    })
  );

export {fetchUser};
