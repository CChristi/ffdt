import {combineEpics} from 'redux-observable';
import {submitForm} from 'root/features/user-management/epics/login';
import {
  initAuthLinkFromLogin,
  initAuthFromLocalStorage,
  saveTokenToLocalStorage,
  clearTokenFromLocalStorage,
  resetAuthLink,
} from 'root/features/user-management/epics/auth';
import {fetchUser} from 'root/features/user-management/epics/user';

const userManagementEpics = combineEpics(
  submitForm,
  initAuthLinkFromLogin,
  initAuthFromLocalStorage,
  saveTokenToLocalStorage,
  fetchUser,
  clearTokenFromLocalStorage,
  resetAuthLink
);

export default userManagementEpics;
