import {ofType} from 'redux-observable';
import {
  authLinkInitialized,
  loginCreated,
  logoutButtonClicked, userFetchError,
} from 'root/features/user-management/actions';
import {filter, ignoreElements, map} from 'rxjs/operators';
import {tap} from 'rxjs';
import {LOCAL_STORAGE_TOKEN_KEY} from 'root/features/user-management/constants';
import {appLaunched} from 'root/actions';

const initAuthLinkFromLogin = (action$, _, {authLink}) =>
  action$.pipe(
    ofType(loginCreated),
    tap(({payload}) => authLink.setToken(payload)),
    map(authLinkInitialized)
  );

const initAuthFromLocalStorage = (action$, _, {authLink}) =>
  action$.pipe(
    ofType(appLaunched),
    map(() => window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY)),
    filter(Boolean),
    tap(jwt => authLink.setToken(jwt)),
    map(authLinkInitialized)
  );

const saveTokenToLocalStorage = action$ =>
  action$.pipe(
    ofType(loginCreated),
    tap(({payload}) => {
      window.localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, payload);
    }),
    ignoreElements()
  );

const clearTokenFromLocalStorage = action$ =>
  action$.pipe(
    ofType(logoutButtonClicked, userFetchError),
    tap(() => window.localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY)),
    ignoreElements()
  );

const resetAuthLink = (action$, _, {authLink}) =>
  action$.pipe(
    ofType(logoutButtonClicked, userFetchError),
    tap(() => authLink.setToken(null)),
    ignoreElements()
  );

export {
  initAuthLinkFromLogin,
  saveTokenToLocalStorage,
  initAuthFromLocalStorage,
  resetAuthLink,
  clearTokenFromLocalStorage,
};
