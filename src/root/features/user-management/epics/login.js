import {ofType} from 'redux-observable';
import {mergeMap} from 'rxjs/operators';
import {
  loginCreated,
  loginFailed,
  loginFormSubmitted,
} from 'root/features/user-management/actions';
import {CREATE_LOGIN} from 'root/features/user-management/graphql/mutations';

const submitForm = (action$, _, {apolloClient}) =>
  action$.pipe(
    ofType(loginFormSubmitted.type),
    mergeMap(({payload: {email, password}}) =>
      apolloClient
        .mutate({
          mutation: CREATE_LOGIN,
          variables: {username: email, password},
        })
        .then(
          ({
            data: {
              login: {jwt},
            },
          }) => loginCreated(jwt)
        )
        .catch(loginFailed)
    )
  );

export {submitForm};
