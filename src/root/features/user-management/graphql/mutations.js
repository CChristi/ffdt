import {gql} from '@apollo/client';

const CREATE_LOGIN = gql`
  mutation login($username: String!, $password: String!) {
    login(input: {identifier: $username, password: $password}) {
      jwt
    }
  }
`;

export {CREATE_LOGIN};
