import {combineReducers} from 'redux';
import userManagementReducer from 'root/features/user-management/reducer';

const rootReducer = combineReducers({
  userManagement: userManagementReducer,
});

export default rootReducer;
