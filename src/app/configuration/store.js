import {configureStore} from '@reduxjs/toolkit';
import epicMiddleware from 'app/configuration/epicMiddleware';
import rootReducer from 'root/reducer';

const store = configureStore({
  reducer: rootReducer,
  middleware: [epicMiddleware],
});

export default store;
