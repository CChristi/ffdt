import {ApolloClient, createHttpLink, InMemoryCache} from '@apollo/client';
import config from 'config';
import authLink from 'app/configuration/apolloLinks/authLink';

const link = createHttpLink({
  uri: config.graphqlEndpoint,
});

const apolloClient = new ApolloClient({
  link: authLink.getLink().concat(link),
  cache: new InMemoryCache(),
});

export default apolloClient;
