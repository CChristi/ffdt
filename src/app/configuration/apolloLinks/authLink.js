import {setContext} from '@apollo/client/link/context';

class AuthLink {
  constructor(scheme = 'Bearer') {
    this.token = null;
    this.scheme = scheme;
  }

  getDecodedPayload() {
    if (!this.token) {
      return null;
    }

    try {
      return JSON.parse(window.atob(this.token.split('.')[1]));
    } catch (e) {
      return null;
    }
  }

  getLink() {
    return setContext((_, {headers}) => {
      const authHeaders = {};

      if (this.token) {
        authHeaders.Authorization = `${this.scheme} ${this.token}`;
      }

      return {
        headers: {
          ...headers,
          ...authHeaders,
        },
      };
    });
  }

  setToken(token) {
    this.token = token;
  }
}

const authLink = new AuthLink();

export default authLink;
