import {createEpicMiddleware} from 'redux-observable';
import apolloClient from 'app/configuration/apolloClient';
import authLink from 'app/configuration/apolloLinks/authLink';

const epicMiddleware = createEpicMiddleware({
  dependencies: {
    apolloClient,
    authLink,
  },
});

export default epicMiddleware;
