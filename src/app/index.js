import {StrictMode} from 'react';
import ReactDOM from 'react-dom';
import Root from 'root/components/Root';
import {ThemeProvider} from '@mui/material';
import {Provider} from 'react-redux';
import theme from 'app/configuration/theme';
import store from 'app/configuration/store';
import epicMiddleware from 'app/configuration/epicMiddleware';
import rootEpic from 'root/epics';
import {appLaunched} from 'root/actions';

epicMiddleware.run(rootEpic);

store.dispatch(appLaunched());

ReactDOM.render(
  <StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Root />
      </ThemeProvider>
    </Provider>
  </StrictMode>,
  document.getElementById('root')
);
