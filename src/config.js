const config = {
  graphqlEndpoint: process.env.REACT_APP_GRAPHQL_ENDPOINT,
  accessControl: {
    loginRoute: '/',
    loginRedirect: '/profile',
    securedRoutes: [/^\/profile/],
  },
};

export default config;
