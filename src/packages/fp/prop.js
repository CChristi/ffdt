const prop = key => obj => obj[key];

export default prop;
